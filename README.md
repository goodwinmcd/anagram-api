## Temporary hosting

Temporarily hosted at http://73.34.29.180:3000/.

Swagger docs at http://73.34.29.180:3000/swagger.

## How to run

docker-compose build to build the containters and then docker-compose up
should be enough to get the application running.

Docker will build a postgres database and initialize the tables. It will then
build the dotnet core application container.

There is a postman suite in the Postman directory. It has postman requests for
each of the endpoints I created in the api.

Swagger docs can be seen at http://localhost:3000/swagger once it is running.

## Other ways to run

If you do not want to use docker than you will need to have a local postgres
database that you can run the pgTableInit.sql script located in the Postgres
directory of this project.

You will need the dotnet sdk to build and launch the application.

You can host the database by building/running the dockerfile located in the postgres
directory and then spinning up the app with docker run.
If the app is run without docker it will look on the localhost by default for
the postgres connection.

## Overview

I thought of several different ways to approach the issue.

1. Brute force - Load all words located in the database into memory, iterate through them adding found anagrams to a list, and then return the list. It would have a run time of O(m * n) with m being the number of words in the database and n being the length of them. This is not a very efficient or scalable solution. It would simplify the data layer though.

2. Determine the anagrams during the SQL query. Some issues with this is that the SQL query would be still be slow. I also do not like to put that kind of responsibility on my data repository layer.

3. Split the database into 26 columns like "hasA, hasB, ..., hasY, hasZ". Each column would contain an integer for how many times that letter appears in the word. This would allow queries to be sped up but only for anagram lookups. However it would cause a ton of overhead in constructing queries and I could see a ton of complexities arising with the database.

4. The method I decided to use comes from the fact that when anagrams are sorted they become the same word. So I would have a second column in my table that is every word sorted. This would allow quick look ups of all groups of anagrams. I also added an index to the new column for quicker lookups.

The architecture of the application is very straight forward. API layer consisting
controllers that calls the service layer. The service layer that contains classes
for doing any non data access actions. And then the data repository layer responsible
for interacting with the database.

I used dapper as my ORM because I like the control it gives you over writing the
SQL vs EF Core that *tries* to do all the work for you but usually ends up
making everything worse.

Inversion of control (Dependancy injection) was the only design pattern I used.
The application was simple enough to not justify the use of any other patterns.

## Design decisions

I separated the controllers, services, and repos into two separate folders. The
enpoints that accomplish core requirements and then the endpoints that address
the optional task listed. I did this to keep a controller from getting too
muddled.

You might notice that every repository method takes in a database context. Why
not just inject the context into the repository layer? The transactions start
on the service layer and they might need to make more than one request to the
repository layer. It would be innefficient to start different transactions every
time a call to a repository method is made. That is why I did it that way.

Using a database instead of an in memory database gave me much more flexibility
with my options to tackle the problem. I ended up putting a lot of responsibility
on the database table to accomplish finding anagrams efficiently. It did add more
overhead to set up the environments but the benefit of flexibility and scalability
made it worth it. Fortunately docker takes away the pain of people have to set
up a local database for the application to work.

## Other things I would of done for production

Unit test. This is almost implied. It's impossible to setup a CICD pipeline if
you do not have good test code coverage to be confident enough to catch breaking
changes during automatic deploy.

METRICS!!!!! I would've setup prometheus and grafana to get an idea of performance.
Without metrics you have no baseline to compare the quality of performance of
your API overtime.

LOGGING AND EXCEPTION HANDLING!!! I would've included more try catch statements
to handle impropper input and unexpected behavior. I also would set up a greylog
instance to collect streams from the docker containers, database, application, and
anything else running on a cluster with the application.

I put those two in caps because I think they generally get overlooked and will
cause major pains in your application down the road if they are not setup properly.

Different environments. I think ideally it would be nice to have a dev environment
where breaking changes can exist, a QA environment that *should* be stable, and then
production environment. I would set up config files for each of these environments
that point to their respective databases, different logging levels, etc.

I would've made config files containing database login information and added it
to gitignore so that it is not exposed in source control.

Validation. I would've spent more time building validation logic for controller
inputs and models.

Better dapper queries. I would have setup the queries in a way that if model
properties change names then it wouldn't require changing the queries.

More experimenting. I would've taken metrics on the different methods mentioned
above to make sure I chose the best solution.

## Expanding features

It would be cool to start adding other controllers that take care of string
algorithms like determining if a word is a palindrome, determining all substrings,
finding combination of strings, etc.

I believe those features would all live under the same domain and would not need to
be broken out into separate services. __IF__ you wanted to use a service driven
architecture then your services would more than likely share the database/word
data. You would want to break that out into it's own service for
accessing/manipulating data. Your other services could then just focus on any
string processing you would want to accomplish on the words in the database.

I'm already starting to picture some wicked natural language processing (NLP)
APIs you could make using that architecture.