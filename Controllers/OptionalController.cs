using System.Collections.Generic;
using ibottachallenge.Models;
using ibottachallenge.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace ibottachallenge.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OptionalController : ControllerBase
    {
        private readonly IOptionalServices _optionalService;

        public OptionalController(IOptionalServices optionalService)
        {
            _optionalService = optionalService;
        }

        /// <summary>
        /// Returns the count of rows in the database, the min word length
        /// of words in the database, the max word length in the database,
        /// the median of word length, and the average word length of all
        /// words in the database
        /// </summary>
        [HttpGet("stats")]
        public ActionResult<Dictionary<string, int>> GetStats()
        {
            var stats = _optionalService.GetStats();
            return Ok(stats);
        }

        /// <summary>
        /// Returns the words that makeup the most anagrams in the database
        /// </summary>
        [HttpGet("MostAnagrams")]
        public ActionResult<IEnumerable<string>> GetMostAnagramsWords()
        {
            var words = _optionalService.GetMostAnagramWords();
            return Ok(new { Anagrams = words });
        }

        /// <summary>
        /// Returns true if all words passed in are anagrams of each other and
        /// false otherwise
        /// </summary>
        [HttpPost("AreAnagrams")]
        public ActionResult<bool> AreTheseWordsAnagrams([FromBody]AddWordRequest words)
        {
            if (words == null || !words.Words.Any())
            {
                return BadRequest("You need to provide a list of words");
            }
            var areAnagrams = _optionalService.AreAllWordsAnagrams(words.Words);
            return Ok(areAnagrams);
        }

        /// <summary>
        /// Returns all words that have a number of anagrams greater than or equal
        /// to the provided integer
        /// </summary>
        [HttpGet("AnagramsAtSize")]
        public ActionResult<IEnumerable<string>> GetAllAnagramsLargerThan([FromQuery] int? size)
        {
            if (size == null)
                return BadRequest("Need to provide size query param");
            var anagrams =  _optionalService.GetMostMatchingAnagrams((int)size);
            return Ok(anagrams);
        }

        /// <summary>
        /// Removes all anagrams of the given word
        /// </summary>
        [HttpDelete("removeAllAnagrams/{wordToRemove}")]
        public ActionResult DeleteAllAnagramsOfWords([FromRoute] string wordToRemove)
        {
            _optionalService.DeleteWordAnagrams(wordToRemove);
            return NoContent();
        }
    }
}