﻿using System.Linq;
using ibottachallenge.Models;
using ibottachallenge.Services;
using Microsoft.AspNetCore.Mvc;

namespace ibotta_challenge.Controllers
{
    [Route("")]
    [ApiController]
    public class AnagramsController : ControllerBase
    {
        private readonly IWordCrudService _wordCrudService;

        public AnagramsController(IWordCrudService wordCrudService)
        {
            _wordCrudService = wordCrudService;
        }

        /// <summary>
        /// Returns all words in the database that are anagrams of the word
        /// passed into the url. Assuming all words will end with .json based
        /// on sample inputs provided in the Readme
        /// </summary>
        [HttpGet("Anagrams/{word}")]
        public ActionResult<string> Get(
            [FromRoute]string word,
            [FromQuery]int? limit)
        {
            word = word.Substring(0, word.Length - 5);
            var anagrams = _wordCrudService.GetAnagrams(word);
            // filter out the original word from return list
            anagrams = anagrams.Where(a => a != word);
            return limit == null
                ? Ok(new {Anagrams = anagrams})
                : Ok(new {Anagrams = anagrams.Take((int)limit)});
        }

        /// <summary>
        /// Adds words to the database
        /// </summary>
        [HttpPost("words.json")]
        public IActionResult Post([FromBody] AddWordRequest words)
        {
            _wordCrudService.AddWords(words.Words);
            return StatusCode(201, "Created");
        }

        /// <summary>
        /// Deletes all rows in the database
        /// </summary>
        [HttpDelete("words.json")]
        public IActionResult DeleteAll()
        {
            _wordCrudService.DeleteAllWords();
            return NoContent();
        }

        /// <summary>
        /// Deletes single word from the database assuming every word passed will
        /// have .json at the end (assumption based off sample input in README)
        /// </summary>
        [HttpDelete("words/{word}")]
        public IActionResult DeleteWord(string word)
        {
            // assuming every word will have .json at the end
            word = word.Substring(0, word.Length - 5);
            _wordCrudService.DeleteWord(word);
            return NoContent();
        }
    }
}
