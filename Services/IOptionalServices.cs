using System.Collections;
using System.Collections.Generic;

namespace ibottachallenge.Services
{
    public interface IOptionalServices
    {
        Dictionary<string, int> GetStats();
        IEnumerable<string> GetMostAnagramWords();
        bool AreAllWordsAnagrams(IEnumerable<string> words);
        IEnumerable<string> GetMostMatchingAnagrams(int limit);
        void DeleteWordAnagrams(string word);
    }
}