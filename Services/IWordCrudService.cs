using System.Collections.Generic;

namespace ibottachallenge.Services
{
    public interface IWordCrudService
    {
        void AddWords(IEnumerable<string> words);
        IEnumerable<string> GetAnagrams(string word);
        void DeleteAllWords();
        void DeleteWord(string word);

    }
}