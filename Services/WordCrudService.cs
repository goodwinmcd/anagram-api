using System.Data;
using System.Collections.Generic;
using System.Linq;
using ibottachallenge.Models;
using ibottachallenge.DataAccess;
using Microsoft.Extensions.Configuration;
using System;

namespace ibottachallenge.Services
{
    public class WordCrudService : IWordCrudService
    {
        private readonly IDbConnection _dbConnection;
        private readonly IWordRepository _wordRepository;
        private readonly IConfiguration _configuration;

        public WordCrudService(
            IDbConnection dbConnection,
            IWordRepository wordRepository,
            IConfiguration configuration)
        {
            _dbConnection = dbConnection;
            _wordRepository = wordRepository;
            _configuration = configuration;
        }

        public IEnumerable<string> GetAnagrams(string word)
        {
            IEnumerable<string> anagrams;
            _dbConnection.Open();
            using(var transaction = _dbConnection.BeginTransaction())
            {
                anagrams = _wordRepository.GetAnagrams(
                    BuildWordModel(word),
                    _dbConnection);
            }
            _dbConnection.Close();
            return anagrams;
        }

        public void AddWords(IEnumerable<string> words)
        {
            var wordModels = words.Select(BuildWordModel);
            _dbConnection.Open();
            using(var transaction = _dbConnection.BeginTransaction())
            {
                foreach (var w in wordModels)
                    _wordRepository.AddWord(w, _dbConnection);

                transaction.Commit();
            }
            _dbConnection.Close();
        }

        private Word BuildWordModel(string word) =>
            new Word
            {
                WordUnsorted = word.ToLower(),
                WordSorted = String.Concat(word.OrderBy(c => c)),
            };

        public void DeleteAllWords()
        {
            _dbConnection.Open();
            using(var transaction = _dbConnection.BeginTransaction())
            {
                _wordRepository.DeleteAllWords(_dbConnection);
                transaction.Commit();
            }
            _dbConnection.Close();
        }

        public void DeleteWord(string word)
        {
            _dbConnection.Open();
            using(var transaction = _dbConnection.BeginTransaction())
            {
                _wordRepository.DeleteWord(word, _dbConnection);
                transaction.Commit();
            }
            _dbConnection.Close();
        }
    }
}