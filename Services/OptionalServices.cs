using System.Collections.Generic;
using System.Linq;
using System.Data;
using ibottachallenge.DataAccess;
using System;

namespace ibottachallenge.Services
{
    public class OptionalServices : IOptionalServices
    {
        private readonly IDbConnection _dbConnection;
        private readonly IOptionalRepository _optionalRepository;
        private readonly IWordCrudService _wordCrudService;

        public OptionalServices(
            IDbConnection dbConnection,
            IOptionalRepository optionalRepository,
            IWordCrudService wordCrudService)
        {
            _dbConnection = dbConnection;
            _optionalRepository = optionalRepository;
            _wordCrudService = wordCrudService;
        }

        public IEnumerable<string> GetMostAnagramWords()
        {
            IEnumerable<string> anagramWords;
            _dbConnection.Open();
            using(var transaction = _dbConnection.BeginTransaction())
            {
                var mostOccuringAnagrams =
                    _optionalRepository.GetMostSortedWords(_dbConnection);

                if (!mostOccuringAnagrams.Any())
                    return mostOccuringAnagrams.Select(a => a.WordSorted);

                anagramWords = _wordCrudService.GetAnagrams(
                    mostOccuringAnagrams.FirstOrDefault().WordSorted);

                transaction.Commit();
            }
            _dbConnection.Close();
            return anagramWords;
        }

        public Dictionary<string, int> GetStats()
        {
            int wordCount;
            int min;
            int max;
            int median;
            int average;

            _dbConnection.Open();
            using(var transaction = _dbConnection.BeginTransaction())
            {
                wordCount = _optionalRepository.GetWordCount(_dbConnection);
                if (wordCount == 0)
                {
                    return BuildStatsDictionary(0, 0, 0, 0, 0);
                }
                min = _optionalRepository.GetMinWordLength(_dbConnection);
                max = _optionalRepository.GetMaxWordLength(_dbConnection);
                median = _optionalRepository.GetMedianWordLength(_dbConnection);
                average = _optionalRepository.GetAverageWordLength(_dbConnection);
                transaction.Commit();
            }
            _dbConnection.Close();
            return BuildStatsDictionary(wordCount, min, max, median, average);
        }

        private Dictionary<string, int> BuildStatsDictionary(
            int count,
            int min,
            int max,
            int median,
            int average)
        {
            return new Dictionary<string, int>
            {
                { "count", count },
                { "min", min },
                { "max", max },
                { "median", median },
                { "average", average},
            };
        }

        public bool AreAllWordsAnagrams(IEnumerable<string> words)
        {
            if (words.Count() == 0)
                return false;

            var anagramMatcher =
                String.Concat(words.ElementAt(0).OrderBy(c => c));

            foreach (var w in words)
                if (String.Concat(w.OrderBy(c => c)) != anagramMatcher)
                    return false;

            return true;
        }

        public IEnumerable<string> GetMostMatchingAnagrams(int limit)
        {
            IEnumerable<string> anagramWords = new List<string>();
            _dbConnection.Open();
            using(var transaction = _dbConnection.BeginTransaction())
            {
                var mostOccuringAnagrams =
                    _optionalRepository.GetMostSortedWords(_dbConnection)
                    .Where(w => w.Count >= limit);

                foreach(var a in mostOccuringAnagrams)
                {
                    var matchingAnagrams = _wordCrudService.GetAnagrams(a.WordSorted);
                    anagramWords = anagramWords.Concat(matchingAnagrams);
                }
                transaction.Commit();
            }
            _dbConnection.Close();
            return anagramWords;
        }

        public void DeleteWordAnagrams(string word)
        {
            var anagrams = _wordCrudService.GetAnagrams(word);
            foreach (var a in anagrams)
                _wordCrudService.DeleteWord(a);
        }
    }
}