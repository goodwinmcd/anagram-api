CREATE TABLE words (
    WordUnsorted TEXT PRIMARY KEY,
    WordSorted TEXT
);

CREATE INDEX ON words(WordSorted);