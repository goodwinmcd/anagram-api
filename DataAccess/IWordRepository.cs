using System.Collections.Generic;
using System.Data;
using ibottachallenge.Models;

namespace ibottachallenge.DataAccess
{
    public interface IWordRepository
    {
        void AddWord(Word word, IDbConnection conn);
        IEnumerable<string> GetAnagrams(Word word, IDbConnection conn);
        void DeleteAllWords(IDbConnection conn);
        void DeleteWord(string word, IDbConnection conn);

    }
}