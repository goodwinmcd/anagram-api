using System.Collections.Generic;
using System.Data;
using ibottachallenge.Models;

namespace ibottachallenge.DataAccess
{
    public interface IOptionalRepository
    {
        int GetWordCount(IDbConnection conn);
        int GetMinWordLength(IDbConnection conn);
        int GetMaxWordLength(IDbConnection conn);
        int GetMedianWordLength(IDbConnection conn);
        int GetAverageWordLength(IDbConnection conn);
        IEnumerable<SortedWordWithCount> GetMostSortedWords(IDbConnection conn);
    }
}