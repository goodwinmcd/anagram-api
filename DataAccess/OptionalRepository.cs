using System.Data;
using Dapper;
using System.Linq;
using System.Collections.Generic;
using ibottachallenge.Models;

namespace ibottachallenge.DataAccess
{
    public class OptionalRepository : IOptionalRepository
    {
        public int GetAverageWordLength(IDbConnection conn)
        {
            var sql = @"SELECT length(wordunsorted)
                FROM words
                ORDER BY length(wordUnsorted) asc";

            var result = conn.Query<int>(sql);
            return result.Sum() / result.Count();
        }

        public int GetMaxWordLength(IDbConnection conn)
        {
            var sql = @"SELECT length(wordunsorted)
                FROM words
                ORDER BY length(wordUnsorted) desc
                LIMIT 1";

            var result = conn.Query<int>(sql);
            return result.FirstOrDefault();
        }

        public int GetMedianWordLength(IDbConnection conn)
        {
            var sql = @"SELECT length(wordunsorted)
                FROM words
                ORDER BY length(wordUnsorted) asc";

            var result = conn.Query<int>(sql);
            return result.ElementAt(result.Count() / 2);
        }

        public int GetMinWordLength(IDbConnection conn)
        {
            var sql = @"SELECT length(wordunsorted)
                FROM words
                ORDER BY length(wordUnsorted) asc
                LIMIT 1";

            var result = conn.Query<int>(sql);
            return result.FirstOrDefault();
        }


        public int GetWordCount(IDbConnection conn)
        {
            var sql = "SELECT COUNT(*) FROM words";

            var result = conn.Query<int>(sql);
            return result.FirstOrDefault();
        }

        public IEnumerable<SortedWordWithCount> GetMostSortedWords(IDbConnection conn)
        {
            var sql = @"SELECT WordSorted, COUNT(WordSorted)
                FROM words
                GROUP BY WordSorted
                ORDER BY COUNT(WordSorted) DESC";

            var result = conn.Query<SortedWordWithCount>(sql);
            return result ?? new List<SortedWordWithCount>();;
        }
    }
}