using System.Data;
using ibottachallenge.Models;
using Dapper;
using System.Linq;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace ibottachallenge.DataAccess
{
    public class WordRepository : IWordRepository
    {
        private readonly ILogger<WordRepository> _logger;

        public WordRepository(ILogger<WordRepository> logger)
        {
            _logger = logger;
        }

        public IEnumerable<string> GetAnagrams(
            Word word,
            IDbConnection conn)
        {
            _logger.LogInformation($"Looking up anagrams for {word.WordUnsorted}");
            var anagramExist = CheckDbForSortedWord(word.WordSorted, conn);

            var sql = @"Select * FROM words
                WHERE WordSorted = @WordSorted";

            if (anagramExist)
            {
                var result = conn.Query<Word>(sql, word);
                return result.Select(w => w.WordUnsorted).ToList();
            }
            else
            {
                _logger.LogInformation($"Word {word.WordUnsorted} does not exist in the database");
                return new List<string>();
            }
        }

        public void AddWord(Word word, IDbConnection conn)
        {
            var wordExist = CheckDbForWord(word.WordUnsorted, conn);

            var sql = @"INSERT INTO words
                (WordUnsorted,
                WordSorted)
                VALUES
                (@WordUnSorted,
                @WordSorted)";

            if (wordExist)
            {
                _logger.LogInformation($"{word.WordUnsorted} already exist in db.");
                return;
            }
            else
            {
                _logger.LogInformation($"Inserting {word.WordUnsorted}");
                conn.Execute(sql, word);
            }
        }

        public void DeleteWord(string word, IDbConnection conn)
        {
            var wordExist = CheckDbForWord(word, conn);

            var sql = @"DELETE FROM words WHERE WordUnsorted=@WordUnsorted";

            if (wordExist)
            {
                _logger.LogInformation($"Deleting word {word}");
                conn.Execute(sql, new { WordUnsorted = word });
            }
            else
            {
                _logger.LogInformation("Word does not exist in database");
            }
        }

        private bool CheckDbForWord(string word, IDbConnection conn)
        {
            var sql = "SELECT WordUnsorted FROM words WHERE WordUnsorted=@WordUnSorted";
            var result = conn.Query<Word>(sql, new { WordUnsorted = word });
            return result.Count() != 0;
        }

        private bool CheckDbForSortedWord(string word, IDbConnection conn)
        {
            var sql = "SELECT WordSorted FROM words WHERE WordSorted=@WordSorted";
            var result = conn.Query<Word>(sql, new { WordSorted = word });
            return result.Count() != 0;
        }

        public void DeleteAllWords(IDbConnection conn)
        {
            var sql = "DELETE FROM words";
            conn.Execute(sql);
        }

    }
}