namespace ibottachallenge.Models
{
    public class Word
    {
        public string WordUnsorted { get; set; }
        public string WordSorted { get; set; }
    }
}