using System.Collections.Generic;

namespace ibottachallenge.Models
{
    public class AddWordRequest
    {
        public IEnumerable<string> Words { get; } = new List<string>();
    }
}