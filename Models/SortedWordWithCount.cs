namespace ibottachallenge.Models
{
    public class SortedWordWithCount
    {
        public string WordSorted { get; set; }
        public int Count { get; set; }
    }
}